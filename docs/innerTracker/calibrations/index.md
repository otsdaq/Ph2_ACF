# Inner Tracker scans and calibrations

Find below a list of documented scans and calibrations.

## Scans

- [PixelAlive](PixelAlive.md)
- [SCurve](SCurve.md)
- [Noise scan](Noise.md)
- [Gain scan](GainScan.md)

## Pixel tuning

- [Latency scan](LatencyScan.md)
- [Injection delay scan](InjectionDelayScan.md)
- [Clock delay scan](ClockDelayScan.md)
- [Threshold adjustment](ThresholdAdjust.md)
- [Threshold equalization](ThresholdEqualization.md)
- [Threshold minimization](ThresholdMinimization.md)
- [Gain optimization](GainOptimization.md)
- [Voltage tuning](VoltageTuning.md)

## Other

- [BER test](BERTest.md)
- [Generic DAC-DAC scan](GenericDacDac.md)
- [Physics](Physics.md)
- [Data read-back optimisation](DataReadBackOptimisation.md)

## Additional information/HOWTOs

- [Module Testing](../ModuleTesting.md)
- [SCC Tuning](../SCCTuning.md)
- [X-talk Studies](XTalk.md)
- [Explanation of some IT-related terms](TermExplanations.md)
